## Task
> Load [the goods](https://mate-academy.github.io/react_dynamic-list-of-goods/goods.json) and show them on the page

1. At first show a button Start on the page
1. After clicking a button show a component `GoodsList` accepting an array of goods and rendering them inside a `<ul>` and hide the button
1. Show `good.name` using a `good.color`(for example `style={{ color: 'red' }}`)
1. Add `Reverse` button to reverse current order of goods
1. Add `Sort alphabetically` button to show goods in alphabetical order
1. Add `Sort by length` button
1. Add `Load red goods` containing only `red` goods
1. Add `<select>` with numbers from 1 to 10. (1 is default). All the previous buttons
  should now show only goods having length >= than selected value. When you change the
  value the items should be immediately rerendered accordingly.
1. (*) `Reset` button should set the default value to the `<select>`